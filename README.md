<div id="header" align="center">
  <h1>Hi there! I'm Nicolas Del Rosario</h1>
  <h3>A highly skilled Front-end Developer professional with a solid understanding of HTML, CSS and JavaScript.</h3>
</div> <br>

<div id="badges" align="center">
<a href="https://www.linkedin.com/in/nicolasdelrosario/" target="_blank">
  <img src="https://img.shields.io/badge/linkedin-171515?style=for-the-badge&logo=linkedin&logoColor=white" alt="Linkedin Badge" />
</a>
<a href="https://github.com/nicolasdelrosario" target="_blank">
  <img src="https://img.shields.io/badge/github-171515?style=for-the-badge&logo=github&logoColor=white" alt="GitHub Badge" />
</a>
<a href="https://twitter.com/nicodelrosar1o" target="_blank">
  <img src="https://img.shields.io/badge/twitter-171515?style=for-the-badge&logo=twitter&logoColor=white" alt="Twitter Badge" />
</a>
</div> <br>

<div id="about-me">
  <h2>| About me</h2> <br>
</div>

```javascript
const nico = {
  pronouns: "he" | "him",
  email: "contact@nicolasdelrosario.com",
  code: [Javascript, Git, HTML, CSS, SCSS, Bootstrap],
  languages: {
    spanish: "Native",
    english: "B1",
    portuguese: "A2"
  },
  hobbies: ["videogames", "guitar", "coffee"],
  goalsThisYear: {
    //read up again 👾
    newLanguage: "Python",
    newTechnology: ["React", "GraphQL"],
    language: {
      english: "B2",
      portuguese: "B1"
    },
    conferences: true,
    personalProjects: true,
    travel: true,
  },
  funFact: "I code so well, that even my cats sits on the keyboard just to watch me work. 😎"
};
```

<div id="projects" align="center">
  <h2 align="left">| Best Projects</h2>
  <table>
    <tr>
      <td>
        <a href="https://github.com/nicolasdelrosario/User-CRUD" target="_blank">
          <img src="https://res.cloudinary.com/dlghcisov/image/upload/v1674072476/projects/User-CRUD/Read-User.png" alt="User CRUD"></img>
          <label>User-CRUD</label> </br>
        </a>
        <figcaption>This project allows users to create, read, update and delete (CRUD) user data.</figcaption>
      </td>
      <td>
        <a href="https://github.com/nicolasdelrosario/Groovy-Clone" target="_blank">
          <img alt="Groovy Clone" src="https://res.cloudinary.com/dlghcisov/image/upload/v1674005371/projects/Groovy/assets/groovy-homepage.png"></img>
          <label>Groovy-Clone</label> </br>
        </a>
        <figcaption>This project is a replica of the Groovy website. The goal is to showcase skills in web development and user interface design.</figcaption>
      </td>
    </tr>
  </table>
</div>

---
